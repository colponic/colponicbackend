from rest_framework.views import APIView
from rest_framework import status
from django.db import IntegrityError

from utils.response import ResponseError, ResponseSuccess, error_handler
from accounts.models.auth_user import AuthUser
from devices.models.user import User


class SignUpView(APIView):
	permission_classes = []

	@error_handler
	def post(self, request):
		username = request.POST['username']
		password = request.POST['password']
		email = request.POST['email']

		try:
			user = AuthUser.objects.create_user(
				username=username,
				password=password,
				email=email
			)
		except IntegrityError:
			return ResponseError("user_with_given_username_already_exists", status.HTTP_400_BAD_REQUEST)

		full_name = request.POST['full_name']
	
		User.objects.create(
			auth_user=user,
			full_name=full_name,
		)

		return ResponseSuccess("user_created", status=status.HTTP_200_OK)
