import json

from django.contrib.auth.signals import user_logged_in
from django.http.response import HttpResponse
from rest_framework.views import APIView
from oauth2_provider.views.base import TokenView
from oauth2_provider.models import get_access_token_model
from oauth2_provider.signals import app_authorized
from utils.response import error_handler

class LoginView(APIView, TokenView):
	permission_classes = []

	@error_handler
	def post(self, request, *args, **kwargs):
		_, headers, body, status = self.create_token_response(request)
	

		if status == 200:
			access_token = json.loads(body).get("access_token")
			if access_token is not None:
				token = get_access_token_model().objects.get(token=access_token)
				user_logged_in.send(sender=token.user.__class__, request=request, user=token.user)
				app_authorized.send(sender=self, request=request, token=token)
		response = HttpResponse(content=body, status=status)

		for k, v in headers.items():
			response[k] = v
		return response