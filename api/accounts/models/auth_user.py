from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

from accounts.managers.auth_user_manager import AuthUserManager


class AuthUser(AbstractUser):
	first_name = None
	last_name = None
	email = models.EmailField(
		_('email address'),
		unique=True,
		blank=True,
		null=True,
	)
	

	REQUIRED_FIELDS = ['email']

	objects = AuthUserManager()

	def __str__(self):
		return self.username