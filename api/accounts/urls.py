from django.urls import path

from accounts.views.login import LoginView
from accounts.views.sign_up import SignUpView


urlpatterns = [
	path('login/', LoginView.as_view()),
	path('sign_up/', SignUpView.as_view()),
]