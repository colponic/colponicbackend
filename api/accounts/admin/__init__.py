from django.contrib import admin

from accounts.models.auth_user import AuthUser

from accounts.admin.auth_user import AuthUserAdmin

admin.site.register(AuthUser, AuthUserAdmin)