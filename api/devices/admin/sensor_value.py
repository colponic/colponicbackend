from django.contrib import admin
from import_export.admin import ExportMixin


from devices.models.sensor_value import SensorValue


class SensorValueAdmin(ExportMixin, admin.ModelAdmin):
	list_filter = (
		'sensor',
		'sensor__device'
	)
	model = SensorValue
