from django.contrib import admin

from devices.models.sensor import Sensor


class SensorAdmin(admin.ModelAdmin):
	list_filter = (
		'device',
	)
	model = Sensor
