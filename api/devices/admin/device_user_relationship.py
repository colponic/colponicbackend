from django.contrib import admin

from devices.models.device_user_relationship import DeviceUserRelationship

class DeviceUserRelationshipAdmin(admin.ModelAdmin):
	model = DeviceUserRelationship