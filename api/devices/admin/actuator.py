from django.contrib import admin

from devices.models.actuator import Actuator


class ActuatorAdmin(admin.ModelAdmin):
	list_filter = (
		'device',
	)
	model = Actuator
