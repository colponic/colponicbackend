from django.contrib import admin

from devices.models.user import User

class UserAdmin(admin.ModelAdmin):
	model = User