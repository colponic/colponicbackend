from django.contrib import admin

from devices.models.device import Device

class DeviceAdmin(admin.ModelAdmin):
	list_filter = (
		'users',
	)
	model = Device