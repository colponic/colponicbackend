from django.contrib import admin

from devices.models.user import User
from devices.admin.user import UserAdmin
from devices.models.device import Device
from devices.admin.device import DeviceAdmin
from devices.models.actuator import Actuator
from devices.admin.actuator import ActuatorAdmin
from devices.models.alarm import Alarm
from devices.admin.alarm import AlarmAdmin
from devices.models.sensor import Sensor
from devices.admin.sensor import SensorAdmin
from devices.models.sensor_value import SensorValue
from devices.admin.sensor_value import SensorValueAdmin
from devices.models.device_user_relationship import DeviceUserRelationship
from devices.admin.device_user_relationship import DeviceUserRelationshipAdmin

admin.site.register(User, UserAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(Actuator, ActuatorAdmin)
admin.site.register(Alarm, AlarmAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(SensorValue, SensorValueAdmin)
admin.site.register(DeviceUserRelationship, DeviceUserRelationshipAdmin)
