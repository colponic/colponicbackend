from django.contrib import admin

from devices.models.alarm import Alarm


class AlarmAdmin(admin.ModelAdmin):
	list_filter = (
		'sensor',
	)
	model = Alarm