from django.db import models
from managers.base_manager import BaseManager

from devices.models.sensor import Sensor


class Alarm(models.Model):
  sensor = models.ForeignKey(
    Sensor,
    on_delete=models.CASCADE
  )
  description = models.CharField(
    max_length=100,
  )
  viewed = models.BooleanField(
    default=False
  )
  created_at = models.DateTimeField(
    auto_now_add=True,
  )

  objects = BaseManager()


  def __str__(self):
    return f'{self.sensor} - {self.description} - {self.created_at}'