from django.db import models


class DeviceUserRelationship(models.Model):
	id = models.BigAutoField(
  	primary_key=True
  )
	user = models.ForeignKey(
		'devices.User',
		on_delete=models.CASCADE
	)
	device = models.ForeignKey(
		'devices.Device',
		on_delete=models.CASCADE
	)
  
	class Meta:
		unique_together = ['user', 'device']
  
	def __str__(self):
		return f'{self.device.name} of {self.user.username}'