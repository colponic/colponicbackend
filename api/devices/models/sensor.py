from django.db import models
from managers.base_manager import BaseManager
from devices.models.device import Device


class Sensor(models.Model):
	device = models.ForeignKey(
		Device,
		on_delete=models.CASCADE
	)
	name = models.CharField(
		max_length=30
	)
	min_value = models.DecimalField(
		null=True,
		max_digits=10,
		decimal_places=2
	)
	max_value = models.DecimalField(
		null=True,
		max_digits=10,
		decimal_places=2
	)

	objects = BaseManager()

	def __str__(self):
		return f'{self.device.name} - {self.name}'
