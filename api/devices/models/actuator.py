from django.db import models
from managers.base_manager import BaseManager
from devices.models.device import Device


class Actuator(models.Model):
	device = models.ForeignKey(
		Device,
		on_delete=models.CASCADE
	)
	name = models.CharField(
		max_length=30
	)
	state = models.BooleanField(
		default=False
	)
	time_on = models.IntegerField(
		default=0
	)
	time_off = models.IntegerField(
		default=0
	)
	objects = BaseManager()

	def __str__(self):
		actual_state = 'OFF'
		if self.state: actual_state = 'ON'
		return f'{self.device.name} - {self.name} - {actual_state}'
