from django.db import models

from accounts.models.auth_user import AuthUser
from managers.base_manager import BaseManager
from utils.random_filename_path import RandomFilenamePath


class User(models.Model):
	auth_user = models.OneToOneField(
		AuthUser,
		on_delete=models.CASCADE,
		related_name='device',
		primary_key=True,
	)
	full_name = models.CharField(
		max_length=30,
	)
	avatar = models.FileField(
		upload_to=RandomFilenamePath('user/avatars/'),
		null=True,
		blank=True,
	)

	objects = BaseManager()

	@property
	def username(self):
		return self.auth_user.username

	def __str__(self):
		return f'{self.full_name} - {self.username}'