from django.db import models
from managers.base_manager import BaseManager
from devices.models.sensor import Sensor


class SensorValue(models.Model):
	sensor = models.ForeignKey(
		Sensor,
		on_delete=models.CASCADE
	)
	value = models.DecimalField(
		max_digits=10,
		decimal_places=2
	)
	unit = models.CharField(
		max_length=30
	)
	reading_date = models.DateTimeField(
		auto_now_add=True,
	)

	objects = BaseManager()

	def __str__(self):
		return f'{self.sensor.name}: {str(self.value)} {self.unit} at {self.reading_date.strftime("%Y-%m-%d %H:%M:%S")}'
