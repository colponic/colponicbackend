from django.db import models
from managers.base_manager import BaseManager

from utils.random_filename_path import RandomFilenamePath
from devices.models.device_user_relationship import DeviceUserRelationship


class Device(models.Model):
  name = models.CharField(
    max_length=30,
  )
  location = models.CharField(
    max_length=30,
  )
  photo = models.FileField(
    upload_to=RandomFilenamePath('devices/photos/'),
    null=True,
    blank=True,
  )
  start_date = models.DateField()
  end_date = models.DateField()
  state = models.BooleanField(default=False)
  
  users = models.ManyToManyField(
		'devices.User',
		through=DeviceUserRelationship
	)
  objects = BaseManager()

  @property
  def username(self):
    return self.auth_user.username

  def __str__(self):
    return f'{self.name} - {self.location}'