from django.urls import path

from devices.views.device import DeviceView
from devices.views.read_sensor import ReadSensorView
from devices.views.save_data_sensor import SaveDataSensorView
from devices.views.update_status_device import UpdateStatusDeviceView
from devices.views.update_actuator_state import UpdateActuatorStateView
from devices.views.actuator_state import ActuatorStateView
from devices.views.alarm import AlarmView

urlpatterns = [
	path('list/', DeviceView.as_view()),
	path('<int:id>', DeviceView.as_view()),
	path('read_sensor/<int:id>', ReadSensorView.as_view()),
	path('save_data_sensor/<int:id>', SaveDataSensorView.as_view()),
	path('update_status_device/<int:id>', UpdateStatusDeviceView.as_view()),
	path('read_actuators/<int:id>', ActuatorStateView.as_view()),
	path('update_actuator_state/<int:id>', UpdateActuatorStateView.as_view()),
	path('alarms/<int:id>', AlarmView.as_view()),
]