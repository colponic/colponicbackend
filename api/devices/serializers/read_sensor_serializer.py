from rest_framework import serializers
from devices.models.sensor_value import SensorValue


class ReadSensorSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = SensorValue
		fields = '__all__'