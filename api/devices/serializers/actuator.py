from rest_framework import serializers
from devices.models.actuator import Actuator


class ActuatorSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Actuator
		fields = '__all__'