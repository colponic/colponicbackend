from rest_framework import serializers
from devices.models.alarm import Alarm


class AlarmSerializer(serializers.ModelSerializer):
	sensor_name = serializers.SerializerMethodField()

	def get_sensor_name(self, obj):
		return obj.sensor.name

	class Meta:
		model = Alarm
		fields = (
			'id',
			'description',
			'viewed',
			'created_at',
			'sensor',
			'sensor_name'
		)