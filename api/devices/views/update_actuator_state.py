from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from devices.models.actuator import Actuator


class UpdateActuatorStateView(APIView):
	permission_classes = []

	def put(self, request, id):
		actuator_state = request.data['state']
		actuator_name = request.data['name']
		actuator = Actuator.objects.get(device_id=id, name=actuator_name)
		actuator.state = actuator_state
		actuator.save()
		return Response('update_successfully', status=status.HTTP_200_OK)