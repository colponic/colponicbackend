from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from utils.response import error_handler, ResponseError
from devices.models.alarm import Alarm
from devices.models.sensor import Sensor
from devices.serializers.alarm import AlarmSerializer


class AlarmView(APIView):
	permission_classes = [permissions.IsAuthenticated]

	@error_handler
	def get(self, request, id=None):
		sensors = Sensor.objects.filter(device_id=id)
		alarms = Alarm.objects.filter(sensor__in=sensors).filter(viewed=False)
		alarms_json = AlarmSerializer(alarms, many=True).data
		return Response(alarms_json, status=status.HTTP_200_OK)