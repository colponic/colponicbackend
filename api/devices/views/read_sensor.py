from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from devices.models.sensor import Sensor
from devices.models.sensor_value import SensorValue
from devices.serializers.read_sensor_serializer import ReadSensorSerializer

class ReadSensorView(APIView):
	permission_classes = [permissions.IsAuthenticated]

	def get(self, request, id):
		sensors = Sensor.objects.filter(device_id=id)
		values = []
		for sensor in sensors:
			sensor_value = SensorValue.objects.filter(sensor_id=sensor.id).order_by('-id')[0]
			values.append(sensor_value)
		sensor_value_json = ReadSensorSerializer(values, many=True).data
		return Response(sensor_value_json, status=status.HTTP_200_OK)


