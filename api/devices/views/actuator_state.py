from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from devices.models.actuator import Actuator
from devices.serializers.actuator import ActuatorSerializer


class ActuatorStateView(APIView):
	permission_classes = [permissions.IsAuthenticated]

	def get(self, request, id):
		actuators = Actuator.objects.filter(device_id=id)
		actuators_json = ActuatorSerializer(actuators, many=True).data
		return Response(actuators_json, status=status.HTTP_200_OK)