from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from devices.models.sensor import Sensor
from devices.models.sensor_value import SensorValue

units = {
	'temp-0': 'ºC',
	'pressure': 'pa',
	'elevation': 'msnm',
	'lux': 'lux',
	'temp-1': 'ºC'
}


class SaveDataSensorView(APIView):
	permission_classes = []

	def post(self, request, id):
		for key, value in request.data.items():
			sensor = Sensor.objects.get(name=key, device_id=id)
			SensorValue.objects.create(sensor_id=sensor.id, value=value, unit=units[key])
		return Response(request.data, status=status.HTTP_201_CREATED)

