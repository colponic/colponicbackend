from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from devices.models.device import Device


class UpdateStatusDeviceView(APIView):
	permission_classes = []

	def post(self, request, id):
		device = Device.objects.get(id=id)
		device.status = True
		device.save()
		return Response(request, status=status.HTTP_201_CREATED)