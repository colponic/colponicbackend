from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from devices.models.device import Device
from devices.serializers.device import DeviceSerializer

class DeviceView(APIView):
	permission_classes = [permissions.IsAuthenticated]

	def get(self, request, id=None):
		if id is None:
			devices = Device.objects.filter(users=request.user.id)
			devices_json = DeviceSerializer(devices, many=True, context={'request': request}).data
			return Response(devices_json, status=status.HTTP_200_OK)
		else:
			device = Device.objects.get(users=request.user.id, id=id)
			device_json = DeviceSerializer(device, context={'request': request}).data
			return Response(device_json, status=status.HTTP_200_OK)