import os

import paho.mqtt.publish as publish


def device_status():
  publish.single(
		topic='colponic/request',
		payload='status',
		hostname=os.environ.get('MQTT_BROKER', None),
		port=int(os.environ.get('MQTT_PORT', None)),
		client_id=os.environ.get('MQTT_CLIENT_ID', None),
		auth={
			'username': os.environ.get('MQTT_USER', None),
			'password': os.environ.get('MQTT_PASS', None)
		}
	)


