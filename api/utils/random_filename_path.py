import os
import uuid

from django.utils.deconstruct import deconstructible


@deconstructible
class RandomFilenamePath(object):

	def __init__(self, sub_path):
		self.path = sub_path

	def __call__(self, _instance, filename):
		extension = filename.split('.')[-1]
		new_filename = '{}.{}'.format(uuid.uuid4(), extension)
		return os.path.join(self.path, new_filename)