#!/bin/bash

set -e

cd ./api/
git checkout main
git pull

source ../../bin/activate
set -o allexport; source .env; set +o allexport
python -m pip install -r requirements.txt && python manage.py migrate --no-input && python manage.py collectstatic --no-input
deactivate


supervisorctl restart colponic 
systemctl reload nginx

